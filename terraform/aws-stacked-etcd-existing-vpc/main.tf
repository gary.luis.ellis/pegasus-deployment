data "aws_vpc" "vpc" {
  id = var.vpc_id
}

data "aws_subnet" "private_subnets" {
  count = length(var.private_subnet_names)

  tags = { Name = var.private_subnet_names[count.index]}
  vpc_id = var.vpc_id
}



module "iam_roles" {
  source = "github.com/garyellis/tf_module_aws_iam_role_k8s"

  name = var.name
}

module "control_plane_sg" {
  source  = "github.com/garyellis/tf_module_aws_security_group"

  description                       = format("%s-control-plane security group", var.name)
  egress_cidr_rules                 = []
  egress_security_group_rules       = []
  ingress_self_security_group_rules = local.control_plane_ingress_self_security_group_rules
  ingress_cidr_rules                = concat(var.ingress_cidr_rules, local.control_plane_ingress_cidr_rules)
  ingress_security_group_rules      = local.control_plane_ingress_security_group_rules
  name                              = format("%s-control-plane",var.name)
  tags                              = var.tags
  toggle_allow_all_egress           = true
  toggle_allow_all_ingress          = false
  toggle_self_allow_all_egress      = false
  toggle_self_allow_all_ingress     = false
  vpc_id                            = var.vpc_id
}

module "userdata" {
  source = "github.com/garyellis/tf_module_cloud_init"

  base64_encode  = true
  gzip           = true
  install_docker = true
}

module "control_plane_nodes" {
  source = "github.com/garyellis/tf_module_aws_instance"

  count_instances                 = var.count_control_plane_nodes
  ami_id                          = var.ami_id
  ami_name                        = var.ami_name
  associate_public_ip_address     = false
  instance_type                   = var.control_plane_instance_type
  iam_instance_profile            = module.iam_roles.controlplane.name
  key_name                        = var.key_name
  name                            = format("%s-control-plane", var.name)
  security_group_attachments      = concat(var.security_group_attachments, list(module.control_plane_sg.security_group_id))
  subnet_ids                      = var.private_subnets     # control plane subnet ids should have their own parameter
  tags                            = merge(var.tags, map(format("kubernetes.io/cluster/%s", var.cluster_name), "owned"))
  user_data_base64                = module.userdata.cloudinit_userdata
}

module "control_plane_lb" {
  source = "github.com/garyellis/tf_module_aws_nlb"

  enable_deletion_protection = false
  internal                   = true
  listeners_count            = local.control_plane_lb_listeners_count
  listeners                  = local.control_plane_lb_listeners
  name                       = var.name
  subnets                    = var.private_subnets # control plane lb should have its own parameter
  target_groups_count        = local.control_plane_lb_target_groups_count
  target_groups              = local.control_plane_lb_target_groups_ip
  target_group_health_checks = local.control_plane_lb_target_group_health_checks
  vpc_id                     = var.vpc_id
  tags                       = var.tags
}

resource "aws_lb_target_group_attachment" "apiserver" {
  count                      = var.count_control_plane_nodes
  target_group_arn           = module.control_plane_lb.target_group_arns[0]
  target_id                  = module.control_plane_nodes.aws_instance_private_ips[count.index]
}

module "control_plane_dns" {
  source = "github.com/garyellis/tf_module_aws_route53_zone"

  create_zone           = false
  name                  = var.dns_zone
  alias_records         = [{ name = var.name, aws_dns_name = module.control_plane_lb.lb_dns_name, zone_id = module.control_plane_lb.lb_zone_id, evaluate_target_health = "true" }]
  alias_records_count   = 1
  zone_id               = var.dns_zone_id
}

module "worker_nodes" {
  source = "github.com/garyellis/tf_module_aws_instance"

  count_instances                 = var.count_worker_nodes
  ami_id                          = var.ami_id
  ami_name                        = var.ami_name
  associate_public_ip_address     = false
  instance_type                   = var.worker_instance_type
  iam_instance_profile            = module.iam_roles.worker.name
  key_name                        = var.key_name
  name                            = format("%s-worker", var.name)
  security_group_attachments      = concat(var.security_group_attachments, list(module.control_plane_sg.security_group_id))
  subnet_ids                      = var.private_subnets
  tags                            = merge(var.tags, map(format("kubernetes.io/cluster/%s", var.cluster_name), "owned"))
  user_data_base64                = module.userdata.cloudinit_userdata
}
