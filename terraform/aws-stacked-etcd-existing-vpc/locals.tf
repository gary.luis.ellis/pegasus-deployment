locals {

  # control plane security group rules
  control_plane_ingress_self_security_group_rules = [
    { desc = "etcd client ",  from_port = "2379", to_port = "2379", protocol = "tcp"},
    { desc = "etcd peer",  from_port = "2380", to_port = "2380", protocol = "tcp"},
    { desc = "apiserver",  from_port = "6443", to_port = "6443", protocol = "tcp"},
    { desc = "flannel/canal",  from_port = "8472", to_port = "8472", protocol = "udp"},
    { desc = "kubelet",  from_port = "10250", to_port = "10250", protocol = "tcp"},
    { desc = "prometheus-kube-scheduler",  from_port = "10251", to_port = "10251", protocol = "tcp"},
    { desc = "prometheus-kube-controller-manager",  from_port = "10252", to_port = "10252", protocol = "tcp"},
    { desc = "prometheus-node-exporter",  from_port = "9100", to_port = "9100", protocol = "tcp"},
    { desc = "node port range",  from_port = "30000", to_port = "32767", protocol = "tcp"},
    { desc = "node port range",  from_port = "30000", to_port = "32767", protocol = "udp"},
  ]
  control_plane_ingress_security_group_rules      = []
  control_plane_ingress_cidr_rules                = [
    { desc = "vpc apiserver (needed by nlb health checks)",  from_port = "6443", to_port = "6443", protocol = "tcp", cidr_blocks = "${data.aws_vpc.vpc.cidr_block}" },
    { desc = "temporary ssh access until mgmt sg exists",  from_port = "22", to_port = "22", protocol = "tcp", cidr_blocks = "${data.aws_vpc.vpc.cidr_block}" },
  ]

  # control plane load balancer
  control_plane_lb_listeners                  = [{ port = "6443", target_group_index = "0" },]
  control_plane_lb_listeners_count            = "1"

  # we use target type ip so control plane nodes can call self lb (apiserver lb)
  # proxy  protocolv2 is not enabled. the apiserver does not directly support it.
  control_plane_lb_target_groups_ip           = [{ name = "apiserver", target_type = "ip", port = "6443",},]
  control_plane_lb_target_group_health_checks = [
    { target_groups_index = "0", protocol = "HTTPS", path = "/healthz", port = "6443", interval = "10", healthy_threshold = "2", unhealthy_threshold = "2" },
  ]
  control_plane_lb_target_groups_count        = "1"

}
