output "control_plane_nodes" {
  value = [
    for i in module.control_plane_nodes.aws_instances[*]:
      {
        user        = "centos"
        key         = "/Users/gary/.ssh/id_rsa"
        hostname    = i.private_dns
        address     = i.private_ip
        instance_id = i.instance_id
        labels      = { "node-role.kubernetes.io/etcd" = "", "node-role.kubernetes.io/master" = "" }
      }
  ]
}

output "worker_nodes" {
  value = [
    for i in module.worker_nodes.aws_instances[*]:
      {
        user        = "centos"
        key         = "/Users/gary/.ssh/id_rsa"
        hostname    = i.private_dns
        address     = i.private_ip
        instance_id = i.instance_id
        labels      = { "node-role.kubernetes.io/worker" = "" }
      }
  ]
}
