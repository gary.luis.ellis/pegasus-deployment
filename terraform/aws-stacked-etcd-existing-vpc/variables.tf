variable "ami_id" {
  type = string
  default = ""
}

variable "ami_name" {
  type = string
  default = ""
}

variable "cluster_name" {
  type = string
  default = "" 
}

variable "count_control_plane_nodes" {
  type = number
  default = 3
}

variable "count_worker_nodes" {
  type = number
  default = 3
}

variable "dns_zone" {
  description = "the private hosted zone name"
  type = string
  default = "ews.works"
}

variable "dns_zone_id" {
  type = string
  default = ""
}

variable "ingress_cidr_rules" {
  description = "a list of ingress security group rules"
  type    = list(map(string))
  default = []
}


variable "control_plane_instance_type" {
  description = "the control plane ec2 instance type"
  type = string
  default = "t2.medium"
}

variable "worker_instance_type" {
  description = "the worker ec2 instance type"
  type = string
  default = "t2.medium"
}


variable "key_name" {
  default = ""
  type = string
}

variable "name" {
  description = "a name or label applied to all resources"
  type = string
}

variable "private_subnets" {
  description = "the target subnets"
  type = list(string)
  default = []
}

variable "private_subnet_names" {
  description = "a list of subnet names. (not implemented)"
  type = list(string)
  default = []
}


variable "security_group_attachments" {
  description = "a list of security groups to attach to the instances"
  type = list(string)
  default = []
}

variable "tags" {
  description = "a map of tags applied to all taggable resources"
  type = map(string)
  default = {}
}

variable "vpc_id" {
  description = "the target vpc"
  type = string
}
